package org.openjfx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ResourceBundle;

public class Formulari implements Initializable {
    @FXML
    private TextField tfNom;

    @FXML
    private TextField tfCognom;

    @FXML
    private TextArea taComentari;

    @FXML
    private ToggleGroup rbsexe;

    @FXML
    private ChoiceBox<String> ChCiutat;

    @FXML
    private ChoiceBox<String> chSO;

    @FXML
    private Spinner<Integer> spHores;

    @FXML
    private DatePicker dpData;
    @FXML
    private Button btGenerarInforme;

    @FXML
    private ImageView imgView;

    @FXML
    void crearInforme() {
        RadioButton rbtt = new RadioButton();

        String fecha = "";

        //si no es null cogemos el valor del DatePicker para mostrarlo
        if (dpData.getValue() != null) {
            fecha = dpData.getValue().toString();
        }

        //Solo si el radioButton no es null asignamos
        if (rbsexe.getSelectedToggle() != null) {
            rbtt = (RadioButton) rbsexe.getSelectedToggle();

        }

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Datos de Formulario");
        alert.setTitle("Datos");


        alert.setContentText("Nombre: " + tfNom.getText() +
                " \n" + "Apellido: " + tfCognom.getText() +
                "\n" + "Comentario: " +
                taComentari.getText() + "\n" +
                "Sexe: " + rbtt.getText() + "\n" +
                "Ciutat: " + ChCiutat.getValue() + "\n" +
                "Sistema Operativo: " + chSO.getValue() + "\n" +
                "Hores davant l'ordinador: " + spHores.getValue() + "\n" +
                "Data d'emplenat del formulari: " + fecha);

        alert.showAndWait();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Image img;

        ChCiutat.getItems().add("Alicante");
        ChCiutat.getItems().add("Castellón");
        ChCiutat.getItems().add("Valencia");
        ChCiutat.setValue("Alicante");

        //Eleccion del SO
        chSO.getItems().add("Linux");
        chSO.getItems().add("Mac");
        chSO.getItems().add("Windows");
        chSO.setValue("Linux");
        img = new Image("Linux.png");
        imgView.setImage(img);


        //para empezar con 1h por defecto
        int initial = 1;

        //Creamos un rango de 1 a 15h
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 15, initial);
        spHores.setValueFactory(valueFactory);


        //escuchador para cambiar la imagen
        chSO.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {

                //llamamos la funcion para cambiar imagen
                seleccionarImagen(t1.intValue());
            }

        });

    }

    public void seleccionarImagen(int tipo) {
        Image img;
        switch (tipo) {
            case 0:
                img = new Image("Linux.png");
                imgView.setImage(img);
                break;

            case 1:
                img = new Image("Mac.png");
                imgView.setImage(img);
                break;
            default:
                img = new Image("Windows.png");
                imgView.setImage(img);
        }


    }


}
