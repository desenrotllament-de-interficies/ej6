package org.openjfx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Login implements Initializable {

    @FXML
    private TextField tfuser;

    @FXML
    private TextField tfpass;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    void Registrar(ActionEvent event) throws IOException {


            FXMLLoader fmxload = new FXMLLoader(getClass().getResource("formularioregistro.fxml"));
            Scene scene = new Scene(fmxload.load());
            scene.getStylesheets().add("css/style.css");
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();


    }

    @FXML
    void login(ActionEvent event) throws IOException {
        //if (tfuser.getText().equals("fabricio")&&tfpass.getText().equals("1234")) {
            FXMLLoader fmxload = new FXMLLoader(getClass().getResource("formulari.fxml"));
            Scene scene = new Scene(fmxload.load());
            scene.getStylesheets().add("css/style.css");
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
       // }
    }
}
